FROM centos:latest

RUN yum install -y python3
RUN python3 -m pip install influxdb Pyserial

COPY owl.py /opt

ENTRYPOINT /opt/owl.py
