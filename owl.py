#!/usr/bin/env python3
import os
import serial
from influxdb import InfluxDBClient

SERIALDEVICE = os.getenv('OWL_DEVICE', '/dev/ttyUSB0')
INFLUXHOST = os.getenv('OWL_INFLUX_HOST', 'localhost')
INFLUXPORT = os.getenv('OWL_INFLUX_PORT', '8086')
INFLUXUSER = os.getenv('OWL_INFLUX_USER', 'root')
INFLUXPASS = os.getenv('OWL_INFLUX_PASS', 'root')
INFLUXDB = os.getenv('OWL_INFLUX_DB', 'owl')


print("Connecting to serial device %s..." % SERIALDEVICE)
ser = serial.Serial(SERIALDEVICE, 250000)
print("Connected!")

print("Connecting to influx host on %s..." % INFLUXHOST)
influx = InfluxDBClient(INFLUXHOST, INFLUXPORT, INFLUXUSER, INFLUXPASS, INFLUXDB)
print("Connected!")

influx.create_database(INFLUXDB)


def parse(inp):
    if inp[0] == 89:
#        print("year: " + str(inp[1]))
#        print("mon: " + str(inp[2] & 63))
#        print("data avail: " + str(inp[2] & 64))
#        print("tariff: " + str(inp[2] & 128))
#        print("day: " + str(inp[3]))
#        print("hour (leave as UTC): " + str(inp[4]))
#        print("min: " + str(inp[5]))
#        print("amps: " + str((inp[9] << 8) | inp[8]))
#        print("costl: " + str((inp[7] << 8) | inp[6]))
#        print("watts: " + str(  ((inp[9] << 8) | inp[8]) * 0.07 * 230  ))
        print("----------")

        json_body = [
                {
                    "measurement": "power_usage",
                    "time": "20{:02d}-{:02d}-{:02d}T{:02d}:{:02d}:00Z".format(inp[1], (inp[2] & 63), inp[3], inp[4], inp[5]),
                    "fields": {
                        "value": int(  ((inp[9] << 8) | inp[8]) * 0.07 * 230  )
                    }
                }
        ]
        try:
            influx.write_points(json_body)
        except:
            print(json_body)

while True:
    z = ser.read(11)
    parse(z)
    ser.write(b'Z')
