# Owl Power Monitor to Influx

Power monitoring exporter into influx. For use with OWL power monitors (https://www.theowl.com/)

The python script `owl.py` will read serial data from the specified device, and export it to an [influxDB](https://www.influxdata.com/) database. 

This data typically updates every 6 seconds, however the resulting granularity of data is 1 minute, see "Known Issues".

See https://sourceforge.net/p/electricowl/discussion/1083264/thread/7f01752f/?limit=25

## Usage

### Python

The python script expects a number of environment variables, as follows

| Variable | Purpose | Default |
|----------|---------|---------|
| OWL_DEVICE | The serial device for the USB-connected device. `dmesg` will typically show the device name when it is connected | /dev/ttyUSB0 |
| OWL_INFLUX_HOST | The hostname for the influxDB server | localhost |
| OWL_INFLUX_PORT | The TCP port for the influxDB server | 8086 |
| OWL_INFLUX_USER | The username to connect to the influxDB server | root |
| OWL_INFLUX_PASS | The password to connect to the influxDB server | root |
| OWL_INFLUX_DB | The database to populate with readings | owl |

### Docker

Running with docker requires the same environment variables as above, plus the serial device must be forwarded to the container

```
docker run [-e ENV=VALUE...] --device=/dev/ttyUSB0 --detach pingue/owl-power-monitor:latest
```


## Known Issues

* The serial data from the device does not include a "seconds" field, meaning all data packets within the same minute will overwrite each other, resulting in granularity of one minute.
